# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
APPNAME	       = repo_tools

DIST_ROOT     := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
TOOLSDIR      := ${DIST_ROOT}tools

DESTDIR       =
PREFIX        = /usr/local
INSTALL       = /usr/bin/install -D
MSGFMT        = /usr/bin/msgfmt
SED           = /bin/sed
bindir        = $(PREFIX)/bin
libdir        = $(PREFIX)/lib
sysconfdir    = $(PREFIX)/etc
datarootdir   = ${PREFIX}/share
datadir       = ${datarootdir}
mandir        = ${datarootdir}/man
zsh_compdir   = $(datarootdir)/zsh/site-functions

SHPP	      = /usr/bin/shpp
override SHPPFLAGS+= 
BUILTIN_SHPP  = ${TOOLSDIR}/shpp
WBUILTIN_SHPP = 0

ifneq ($(WBUILTIN_SHPP),0)
SHPP          = $(BUILTIN_SHPP)
endif
