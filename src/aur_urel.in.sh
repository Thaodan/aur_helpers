#!/bin/sh
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#!/bin/sh
#\\include msg.in.sh

appname=${0##*/}



print_help()
{
    cat <<EOF
$appname - help
 -c commit urev
 needs reason either by config file or by
 -r <reson> reason to commit
 -f <file> use file instead of PKGBUILD 
EOF
}

conffile="$HOME/.config/aur_urel"
if [ -e "$conffile" ] ; then
    . "$confile"
fi


# parse input

# c = commit
# r = commit reason
# f = file
# h = help
while getopts hcr:f: arg ; do 
    case $arg in
        c)
	    if [ ! $auto_git ] ; then
		auto_git=1
	    else
		unset $auto_git
	    fi
	    ;;
	f) PKGBUILD="$OPTARG" ;;
	r) auto_git_reason="$OPTARG";; #TODO max reason a opt arg to -c and use generic message if not set
	h|?) print_help;exit ;;
    esac
done



# check for PKGBUILD

if [ "$PKGBUILD" ] ; then
    # test if PKGBUILD is in an other dir
    # else asume its in $PWD
    pkgdir="$( dirname $PKGBUILD )"
    if [ "$pkgdir" ] ; then 
	cd "$pkgdir"
	PKGBUILD=$(basename $PKGBUILD)
    fi
elif [ ! -e $PWD/PKGBUILD ] ; then
    error 'no pkgbuid present or -f given'
    exit 1
else
    PKGBUILD=PKGBUILD
fi

# TODO  add hooks here
# get current pkgrel
pkgrel=$(grep '^pkgrel=*' "$PKGBUILD"| sed 's/pkgrel=//')
new_pkgrel=$(($pkgrel + 1 ))

sed -i $PKGBUILD -e "s/pkgrel=$pkgrel/pkgrel=$new_pkgrel/"

# do auto commit if enabled

if [ $auto_git ] ; then
    if  [ "$auto_git_reason" ] ; then
	mksrcinfo # updated .SRCINFO before commit
	git commit $PKGBUILD .SRCINFO -m "urel: $auto_git_reason"
    else
	error "no reason for auto git commit"
        exit 1
    fi
fi
