# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#\\include 'msg.in.sh'

#\\define AUR_RPC_REQUEST_FILE $aur_jsondir/${aur_rpc_request_id}_rpc.json
#\\define AUR_RPC_URL https://aur.archlinux.org/rpc.php
#\\define AUR_RPC_VER 5

#if [ "$aur_jsondir" = "" ] ; then
#    warning "please set aur_jsondir before loading this"
#fi

getversionedpkgs() {
    downloadjson ${deps[@]}
    depsAname=($(getjsonvar "Name"))    # return sorted results
    depsAver=($(getjsonvar "Version"))
    depsAood=($(getjsonvar "OutOfDate"))
    depsArepo=("${depsAname[@]/#/aur/}" )
    for ((i=0; i<${#depsAname[@]}; i++)); do
        depsQver[$i]=$(expac -Qs '%v' "^${depsAname[$i]}$")
        [[ -z "${depsQver[$i]}" ]] && depsQver[$i]="#"  # avoid empty elements shift
        [[ -n "$(echo "${depsAname[$i]}" | grep -E "\-(cvs|svn|git|hg|bzr|darcs)$")" ]] && depsAver[$i]=$"latest"
    done
    if [[ -n "${repodepspkgs[@]}" ]]; then
        repodepspkgs=($(expac -S -1 '%n' "${repodepspkgs[@]}" | LC_COLLATE=C sort -u))
        repodepsSver=($(expac -S -1 '%v' "${repodepspkgs[@]}"))
        repodepsQver=($(expac -Q '%v' "${repodepspkgs[@]}"))
        repodepsSrepo=($(expac -S -1 '%r/%n' "${repodepspkgs[@]}"))
    fi
}

aur_request()
# description: start request in $aur_jsondir
# arguments: pkgname(s)
# returns: request id
{
    
    local aur_rpc_request_id urlencodedpkgs urlargs

    while aur_rpc_request_id=$RANDOM ; do
        if [ !  -e "$aur_jsondir/$aur_rpc_request_id" ] ; then
            break
        fi
    done

    urlencodedpkgs=($(sed 's/+/%2b/g' <<< "$@" | sed 's/@/%40/g')) # pkgname consists of alphanum@._+-
    urlargs="$(printf "&arg[]=%s" "${urlencodedpkgs[@]}")"
    if ! curl -sfg --compressed -C 0 "@AUR_RPC_URL@?type=multiinfo$urlargs&v=@AUR_RPC_VER@" -o "@AUR_RPC_REQUEST_FILE@" ; then
        AUR_RPC_ERROR="Could not connect to the AUR"
        return 1
    fi

    if grep -q "\"resultcount\":0" @AUR_RPC_REQUEST_FILE@ ; then
        AUR_RPC_ERROR="No pkg found"
        return 1
    fi

    echo "$aur_rpc_request_id"
}

aur_request_var()
# description: get jsonvar from request
# arguments: requestid
# returns: contents of variable
{
    json_reformat < "@AUR_RPC_REQUEST_FILE@" | tr -d "\", " | grep -Po "$1:.*" | sed -r "s/$1:/$1#/g" | awk -F "#" '{print $2}'
}
aur_request_array()
# description: get jsonarray from request
# arguments: requestid
# returns: name of array
{    
    json_reformat < "@AUR_RPC_REQUEST_FILE@" | tr -d "\", " | sed -e "/^$1/,/]/!d" | tr '\n' ' ' | sed "s/] /]\n/g" | awk -F ":" '{print $2}' | tr -d '[]"' | tr -d '\n'
}
aur_request_arrayvar()
# description: get jsonarrayvar from request
# arguments: requestid
# returns: contents of variable
{
    json_reformat < "@AUR_RPC_REQUEST_FILE@" | tr -d "\", " | sed -e "/Name:$2/,/}/!d" | sed -e "/^$1/,/]/!d" | tr '\n' ' ' | awk -F ":" '{print $2}' | tr -d '[]'
}
