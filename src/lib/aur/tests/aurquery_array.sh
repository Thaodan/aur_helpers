#!/bin/bash
#\\include 'var.in.sh'
#\\include 'aur/rpc.in.sh'
#\\include 'tests/tests.sh'
aur_jsondir=$(mktemp -d)
aur_rpc_request_id=$(aur_request war)
name=$(aur_request_var "Name")
version=$(aur_request_var "Version")

test_compare_ok ! $name = wargus 
test_compare_ok ! $version = 2.4.1-1 

#cat @AUR_RPC_REQUEST_FILE@
rm -rf $aur_jsondir
