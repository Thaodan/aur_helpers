# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0

: "${cleanup_files:?Please set \$cleanup_files}"

#\\ifdef CLEANUP_SUDO
#\\define CLEANUP_SUDO_STR sudo
#\\endif

cleanup()
{
    # shellcheck disable=SC2086
    # Note: Don't word split here
    @CLEANUP_SUDO_STR@ rm -rf ${cleanup_files:?}
}

for signal in TERM HUP QUIT; do
    trap "cleanup; exit 1" $signal
done
unset signal
trap "cleanup; exit 130" INT
# shellcheck disable=SC2154
# Note: The variable is supposed to be set when the trap is executed
trap 'last_exit=$?;cleanup; exit $last_exit' EXIT
