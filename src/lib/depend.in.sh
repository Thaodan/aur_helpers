# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0

reset_depend_state()
# usage: reset_depend_state
# desc: Reset state of depend function, to be overriden by caller of depend()
{
    return
}

depend()
# usage: depend <file> [<chainload>] [<chainload_onerror]
# desc:  Load file and execute chainload if defend
#        Chainload is executed for all following depends.
#        If chainload_onerror is defined it is executed in
#        case of an error instead of just return
{
    local file=$1; shift

    if [ $1 ] ; then
        local chainload=$1
        shift
    fi

    if [ $1 ] ; then
        local chainload_onerror=$1
        shift
    fi

    local IFS dir abs_file

    case $file in
        ./*)
            depend_path=$depend_path:"$PWD"
            ;;
        */*)
            depend_path=$depend_path:$(dirname $file)
            ;;
    esac

    case $file in
        /*)
            source "$file"
            if [ "$chainload" ] ; then
                verbose "$file: $chainload"
                "$chainload" "$abs_file"
                chainload_state=$?
            fi
            reset_depend_state
            return ${chainload_state:-$?}
            ;;
        *)
            IFS=:
            for dir in $depend_path ; do
                unset IFS

                if [ -e "$dir/$file" ] ;then
                    abs_file="$dir/$file"

                    . "$abs_file"

                    if [ "$chainload" ] ; then
                        verbose "$file: $chainload"
                        "$chainload" "$abs_file"
                        chainload_state=$?
                    fi
                    reset_depend_state

                    return ${chainload_state:-$?}
                else
                    IFS=:
                fi
            done
    esac


    if [ $chainload_onerror ] ; then
        $chainload_onerror
    else
        @APP_DOMAIN@_FILE_NOT_FOUND="$file"
        return 1
    fi
}
