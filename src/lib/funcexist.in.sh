# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0

#\\ifndef FUNCEXIST_H
#\\define FUNCEXIST_H
funcexist() {
    case "$(LANG=C type -- "$1" 2>/dev/null)" in
        *function*) return 0 ;;
    esac
    return 1
}

#\\endif
