get_protocol()
# desc: extract the protocol from a source entry - return "local" for local sources
# taken from arch linux makepkg
{
    local proto
    if [[ $1 = *://* ]]; then
        # strip leading filename
        proto="${1#*::}"
        printf "%s\n" "${proto%%://*}"
    elif [[ $1 = *lp:* ]]; then
        proto="${1#*::}"
        printf "%s\n" "${proto%%lp:*}"
    else
        printf "%s\n" local
    fi
}
