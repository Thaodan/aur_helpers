#\\import msg.in.sh
lock() {
    # Only reopen the FD if it wasn't handed to us
    local fd_real
    fd_real=$(readlink /dev/fd/"$1") || true
    if [ -z "$fd_real" ] || [ "$fd_real" = "$2" ]; then
        mkdir -p -- "$(dirname -- "$2")"
        eval "exec $1>$2"
        echo "$$" > "$2"
    else
        return $?
    fi
    if ! flock --nonblock "$1"; then
        msg "Busy ${*}"
        flock --exclusive "$1"
        msg "Done"
    fi
}

unlock()
{
    eval "exec $1>&-"
}
