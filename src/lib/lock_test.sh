#!/bin/sh
#\\include locals.shh
#\\import "test/test_base.in.sh"
#\\import "cleanup.in.sh"
#\\import "lock.in.sh"


tmp_file_1=$(mktemp)
tmp_file_2=$(mktemp)

cleanup_files="$tmp_file_1 $tmp_file_2"

wait_and_unlock()
# Wait x seconds and unlock fd
{
    sleep "$1"
    unlock "$2"
}


test_case @ERROR_BIT_OK@ simple-lock-test <<EOF
lock 9 "$tmp_file_1"
unlock 9
EOF

test_case @ERROR_BIT_OK@ SUB wait-lock-test <<EOF
lock 9 "$tmp_file_1"
wait_and_unlock 10 9 &
lock 9 "$tmp_file_1"
unlock 9
EOF

test_case @ERROR_BIT_FAIL@ twice-used-fd-test <<EOF
lock 9 "$tmp_file_1"
lock 9 "$tmp_file_2"
EOF

test_case @ERROR_BIT_OK@ lock-from-another-process-test <<EOF
(
  lock 9 "$tmp_file_1"
  wait_and_unlock 10 9 &
)

lock 9 "$tmp_file_1"
unlock 9

wait $!
EOF
