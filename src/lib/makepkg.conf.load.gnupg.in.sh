# Copyright 2018 - 2022, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0

load_makepkg_conf_gpgkey()
{
    if [ -e "${MAKEPKG_CONF:-$HOME:~/.makepkg.conf}" ] ; then
        if grep -q ^GPGKEY "${MAKEPKG_CONF:-$HOME:~/.makepkg.conf}" ; then
            GPGKEY=$(grep ^GPGKEY "${MAKEPKG_CONF:-$HOME:~/.makepkg.conf}" |sed -E -e 's/.*=//' -e 's/(^"|"$)//g')
            export GPGKEY
        fi
    fi
}
