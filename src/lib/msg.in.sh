# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
error()
{
    warning "$@"
    return 1
}

warning()
{
    msg "$1" $(( ${2:-0} + 2 ))
}

verbose()
{
    [ "$verbose" ] && warning "$@"
}

msg()
# usage: msg <msg> [<mode>]
# desc: print msg with mode
# modes:
# 1      don't print new line
# 2      print to stderr
# >3     do both
# 0      do none of that (default)
{

    if [ "${2:-0}" -ge 2 ] ; then
        op=2
    fi
    # shellcheck disable=SC2039
    # note: not valid here was we use $op to set the fd of our message
    printf %s "$1"     >&${op:-1}
    if [ ! "${2:-0}" -eq 1  ] && [ ! "${2:-0}" -ge 3 ]; then
        # shellcheck disable=SC2039
        # note: not valid here was we use $op to set the fd of our message
        printf '\n' >&${op:-1}
    fi
}
