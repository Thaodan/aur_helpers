# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
pkg_add()
{
    cd "$repo_dir" || exit 1
    if [ ! -e "$1" ] ; then
        git submodule add "ssh://aur@aur.archlinux.org/$1.git"
        git commit .gitmodules "$1" -m"$1: added pkg"
    else
        PKG_ADD_ERROR="pkg $1 already exits"
        return 1
    fi
}
