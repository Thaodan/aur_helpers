#!/bin/bash
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#\\include locals.shh
#\\include 'msg.in.sh'
#\\include 'funcexist.in.sh'
pkg_add_init()
# $1 = backend
{
    local backend=$1; shift
    local target_repo="$1"; shift

    if [ -e "@ADD_BACKEND_DIR@/$backend" ] ; then
        source "@ADD_BACKEND_DIR@/$backend"
    else
        return 1
    fi

    if funcexist __pkg_add_init ; then
        __pkg_add_init "$@"
    fi
}

pkg_add_backends()
{
    local backend
    for backend in "@ADD_BACKEND_DIR@"/* ; do
        echo $backend
    done
}
