#!/bin/bash
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#!/bin/bash


TEMPLATE_PKGBUILD=/usr/share/pacman/PKGBUILD.proto


__pkg_add_init()
{
    tmpl_pkgbuild_file="${1:-$TEMPLATE_PKGBUILD}"
}

pkg_add()
{
    cd "$target_repo" || exit 1
    for pkg in "$@" ; do
        (
            if [ ! -e "$pkg" ] ; then
                mkdir "$pkg"
                cd "$pkg" || exit 1
                git init
                #\\if 0
                for git_remote in ${git_remotes[*]} ; do #TODO
                    git remote add $git_remote
                done
                #\\else
                msg "pkg.db.template.$pkg: Git remote creation not implemented please setup by hand and add it to the main project"
                #\\endif
                cp "$tmpl_pkgbuild_file" ./PKGBUILD
            else
                PKG_ADD_ERROR="pkg $pkg already exits"
                return 1
            fi
        )
    done
}
