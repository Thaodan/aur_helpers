#!/bin/bash
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#!/bin/bash
#\\include locals.shh
#\\import 'var.in.sh'
#\\import 'aur/rpc.in.sh'
#\\import 'cleanup.in.sh'

pkg_db_get()
# $1 = pkg
{
    local pkg=$1; shift
   if [ ! -e $db/pkgs/$pkg ] ; then
       if  ! __pkg_db_aur_cache  "$pkg"; then
           error "pkg not found"
       fi
   else
       echo $db/pkg/$pkgs
   fi   
}

__pkg_db_init()
# $1 = db type
{
    local db_type=$1; shift

    case $db_type in
        local|remote) : ;;
        *) error "wrong db_type" ;;
    esac
    var $db/type=$db_type

    mkdir $db/pkgs
    mkdir $db/raw
}
__pkg_db_aur_cache()
{
    local pkg=$1; shift
    local depend aur_rpc_request_id
    
    aur_jsondir=$db/raw

    if ! aur_rpc_request_id=$(aur_request "$pkg") ; then
        PKG_DB_ERROR="$AUR_RPC_ERROR"
        return 1
    fi

    var $db/pkgs/$pkg/version=$(aur_request_var "Version")
    var $db/pkgs/$pkg/name=$(aur_request_var "Name")

    mkdir -p "$db/pkgs/$pkg/depends"
    for depend in $(aur_request_arrayvar "Depends") ; do
        touch $db/pkgs/$pkg/depends/$depend
    done

    mkdir -p "$db/pkgs/$pkg/provides"
    for depend in $(aur_request_arrayvar "Provides") ; do
        touch $db/pkgs/$pkg/provides/$depend
    done
}
