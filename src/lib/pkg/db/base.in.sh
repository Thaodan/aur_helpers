#!/bin/bash
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#\\include locals.shh
#\\include 'msg.in.sh'
#\\include 'funcexist.in.sh'
pkg_db_init()
# $1 = backend
{
    local backend=$1; shift
    
    if [ -e "@DB_BACKEND_DIR@/$backend" ] ; then
        mkdir -p "@DB_DIR@/$backend"
        db="@DB_DIR@/$backend"
        source "@DB_BACKEND_DIR@/$backend"
    fi

    if funcexist __pkg_db_init ; then
        __pkg_db_init "$@"
    fi
}

pkg_db_backends()
{
    local backend
    for backend in "@DB_BACKEND_DIR@"/* ; do
        echo $backend
    done
}
