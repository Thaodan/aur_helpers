#!/bin/bash
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#!/bin/bash
#\\include locals.shh
#\\import pkg/db/base.in.sh

pkg_resolve()
# desc: resolve pkg
# usage: pkg_resolve <backend> <pkgs>
# returns: var db with pkgs
{
    local pkg, mode

    if [ ! "$db" ] ; then
       # only init db once
        local db=$(pkg_db_init $1)
        shift
    fi
    
    for mode in depend optdepends makedepends ; do
        while [ ! $# = 0 ] ; do
            if pkg=$(pkg_db_get "$1") ; then
                if [ -e "$pkg"/$mode ] ; then
                    for depend_pkg in "$pkg"/$mode/* ; do
                        depend_pkg=${depend_pkg##*/} # ${0##*/}
                        pkg_resolve "$depend_pkg"
                    done
                fi
            fi
            shift
            echo "$pkg"
        done
    done
}
