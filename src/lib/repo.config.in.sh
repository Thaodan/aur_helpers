# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#\\include locals.shh

#\\import depend.in.sh
#\\import getprotocol.in.sh
#\\import msg.in.sh

#\\import repo.config.vars.in.sh

try_find_confin_gitrepositoryroot()
{
    # check config
    if [ ! "$target_repo" ] ; then
        target_repo="$(git rev-parse --show-toplevel)/.@APP_DOMAIN.conf"
        if [ ! -e "$(git rev-parse --show-toplevel)/.@APP_DOMAIN.conf" ] ; then
            error "Please supply config file, either in the repo as" 1
            error "config file with -c <config file>"
            exit 1
        fi
    fi
}

# check if current repo config is valid
repo_valid_check()
{
    local nwords
    for word in $(repo_req_keywords) ; do
        if eval "[ ! \$$word ]" ; then
            nwords+=($word)
        fi
    done

    if [  ${#nwords} -gt 0 ] ; then
        error "${nwords[*]} not defined in current template tree"
        exit 1
    fi
}

reset_depend_state()
{
    local word
    for word in $(repo_req_keywords) ; do
        unset $word
    done
}

add_repo_pacman_conf()
{
    repo_valid_check
    local protocol statement=Include word
    # save instance repo variables
    # as local variant
    for word in $(repo_req_keywords) ; do
        local $word="$(eval "echo \$$word")"
    done

    if [ "$repo_url" = "mirrorlist" ] ; then
        repo_url=/etc/pacman.d/mirrorlist
    else
        if [ "$(get_protocol "$repo_url")" = local ] ; then
            local statement=Server
            protocol=file://
        fi
        case "$repo_url" in
             *%arch%*) repo_url=$(echo "$repo_url"| sed -e "s|%arch%|\$arch|") ;;
        esac
    fi
    cat >> "@TMP_DIR@/pacman.conf" <<EOF
[$repo_name]
$statement = ${protocol}${repo_url}
EOF
    target_repo_name="$repo_name"
}
