# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
include ../../../src.base.mk
BACKEND ?=
MODULE ?=
include ../module.mk

all: $(SCRIPTS)

include $(srcdir)/src.body.mk

install: $(SCRIPTS)
	$(INSTALL) -dm755 $(libdir)/$(APPNAME)/$(MODULE)/$(BACKEND)
	$(INSTALL) -Dm644 $(^) $(libdir)/$(APPNAME)/$(MODULE)/$(BACKEND)

.PHONY: clean install
