
#\\import msg.in.sh

#\\define ERROR_BIT_OK 0
#\\define ERROR_BIT_FAIL 1

test_case()
# usage: test_case ERROR_BIT <desc> <command>
# desc: run test case expect return status ERROR_BIT
{
    local error_bit="$1"
    shift
    msg "Running $*"
    (
        set -e
        . /dev/stdin
    )
    ret=$?
    if [ ! $ret -ge $error_bit ] ; then
        error "$1 failed with $ret"
    fi
    msg "Test Done"
}
