# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#\\ifndef VARDB_DIR
#\\define VARDB_DIR
#\\endif


var()
# usage: var var[=content]
# description: set var to content if =content is not given, output content of var
#              vars can be put in an other by using / just like when creating dirs
{
    case $1 in
	*=|*=*)
	    local __var_part1=$( echo "$1" | sed -e 's/=.*//' -e 's/^[+,-]//' )
	    local __var_part2=$( echo "$1" | sed -e 's/.*.=//' )
	    local __var12=@VARDB_DIR@$__var_part1
	    mkdir -p ${__var12%/*}
	    case $1 in
		*+=*)
		    if [ -d @VARDB_DIR@$__var_part1 ] ; then
			printf  -- $__var_part2 > @VARDB_DIR@$__var_part1/\  $((
				$( echo @VARDB_DIR@$__var_part2/* \
				    | tail  | basename ) + 1 ))
		    else
			printf -- "$__var_part2" >> @VARDB_DIR@$__var_part1
		    fi
		    ;;
 		*-=*) false ;;
                *)  printf  -- "$__var_part2" > @VARDB_DIR@$__var_part1 ;;
	    esac
	    ;;
	*)
	    if [ -d @VARDB_DIR@$1 ] ; then
		local __var_dir_content=$(echo @VARDB_DIR@$1/*)
		shpp_var_oifs=$IFS
		IFS=" "
		for __var_dir in $__var_dir_content ; do
		    echo ${__var_dir##*/}
		done
		IFS=$shpp_var_oifs
	    elif [ -e @VARDB_DIR@$1 ] ; then
		cat @VARDB_DIR@$1
	    else
		return 1
	    fi
	    ;;
    esac
}

unvar()
# usage: unvar <var>
# desription: remove var
{
    rm -rf @VARDB_DIR@$1
}

link() {
    ln -s @VARDB_DIR@$1 @VARDB_DIR@$2
}
