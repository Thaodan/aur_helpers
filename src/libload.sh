#!/bin/bash
libload()
{
    local guardvar="$(sed -e 's|/|D|g' -e "s|\\.|D|g" <<< $1)"
    
    # check include guard
    if [ ! -n "$(eval echo \$LOADED_$guardvar)" ] ; then
        source "$1"
        eval "LOADED_$guardvar=1"
    fi
}
