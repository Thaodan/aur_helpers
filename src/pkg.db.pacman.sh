#!/bin/sh


pkg_get()
# $1 = pkg
{
    local pkg=$1; shift
   if [ -e $db/pkgs/$pkg] ; then
       if  ! __pkg_db_pacman_cache ; then
           error "pkg not found"
       fi
   fi
        
}

__pkg_db_init()
# $1 = db type
{
    local db_type=$1; shift

    case $db_type in
        local|remote) : ;;
        *) error "wrong db_type" ;;
    esac
    var $db/type=$db_typey
}
__pkg_db_pacman_cache()
{
    echo "$stub"
}
