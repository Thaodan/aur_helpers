#!/bin/bash -e
# Copyright 2018 - 2022, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0

#\\include locals.shh
#\\import msg.in.sh
#\\import repo.config.in.sh
#\\import makepkg.conf.load.gnupg.in.sh

move()
# improved mv to detect if argument is a symlink
{
    # file = $1
    # target = $2
    if [ -L "$1" ] ; then
	mv "$(readlink "$1" )" "$2"
	rm "$1"
    else
	mv "$1" "$2"
    fi
}

move_dry()
{
    local err_count=0
    while [ ! $# = 0 ] ; do
        if [ -L "$1" ] ; then
            if [ ! -e "$(readlink "$1" )" ] ; then
                error "cannot stat '$1': No such file or directory"
                ((err_count++))
            fi
        else
            if [ ! -e "$1" ] ; then
                error "cannot stat '$1': No such file or directory"
                ((err_count++))
            fi
        fi      
        shift
    done

    return $err_count
}

save_root_repo()
{
    target_repo_name="$repo_name"
    target_repo_url="$repo_url"
}

display_help()
{
    cat <<EOF
$appname - help
-d set repo_dir
-n set name of repo 
   if name differs between name of repo dir folder
-s sign repo
-D just do dry run and exit
EOF
}
args=d:n:k:hsD
extra_args=( ) # extraargs for repo-add
while getopts $args arg ; do
    case $arg in
	d) # repo dires
	    target_repo_url="$OPTARG"
	    ;;
	n) # repo name
	    # if name differs between name of repo dir folder
	    target_repo_name="$OPTARG"
	    ;;
	k)
	    repo_key="$OPTARG" ;;
	s) sign=t ;; # sign db
        v) verify=t ;;
        D) dry_run=t;;
        h) display_help;exit 0 ;;
    esac
done
shift $(($OPTIND - 1 ))

# if repo name is not set use basename $target_repo_url
if [ ! $target_repo_name ] ; then
    target_repo_name=${target_repo_url##*/}
    arches="x86_64 i686"
    # guessed repo name is the name of an arch use the name
    # of the dir below as target_repo_name
    for arch in $arches ; do
        if [ "$target_repo_name" = $arch ] ; then
            # FIXME test this
            target_repo_url_name=${target_repo_url%/*}
            target_repo_name=${target_repo_url_name##*/}
            break
        fi
    done
else
    if [ ! "$target_repo_url" ] ; then
        if ! depend "$target_repo_name" save_root_repo; then
            error "\$target_repo_url not given and no conf for $target_repo_name"
            exit 1
        fi
    fi
fi

[ "$sign" ] && extra_args+=('--sign')
[ "$verify" ] && extra_args+=('--verify')

if [ "$#" -eq 0 ] ; then
    error "No packages to add"
    exit 1
fi

# do dry run first
for file in "$@" ; do
    # check if all targets exist
    move_dry "$file"
    err_count=$((${?:-0}+${err_count:-0}))
    if [ $sign ] ; then
	move_dry "$file.sig"
    fi
done

if [ ${err_count:-0} -gt 0 ] ;then
    exit $err_count
fi

load_makepkg_conf_gpgkey

if [ ! "$dry_run" ] ; then
    # move packages to target_repo_url
    for file in $* ; do
        move $file "$target_repo_url"/${file##*/}
        if [ $sign ] ; then
	    move $file.sig "$target_repo_url"/${file##*/}.sig
        fi
    done
    (
        cd "$target_repo_url" || exit 1
        
        for file in  $* ; do
            repo-add ${extra_args[*]} $target_repo_name.db.tar.xz ${file##*/}
        done
    )
fi
