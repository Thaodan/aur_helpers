#!/bin/bash
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#!/bin/bash

#\\include locals.shh
#\\include msg.in.sh
#\\include config.in.sh
#\\import pkg/db/base.in.sh
#\\import pkg/add/base.in.sh

usage()
{
    cat <<EOF
usage: $appname -b <backend> or -t [-r <repo directory>]
description: add src pkg to repo from backend or template

-b <backend> Name of the repo that is created
-t           Create template pkg
-r <repo>    Use repo in directory <repo>
EOF
}

opts=b:htr:

while getopts $opts arg ; do
    case $arg in
        b) backend=$OPTARG;;
        t) backend=template;;
        r) target_repo="$OPTARG";;
        h) usage; exit 0;;
        ?) usage; exit 1;;
    esac
done
shift $(( $OPTIND - 1 ))

if [ ! -e "${target_reopo:-$PWD}/.git" ] ; then
    error "Not inside git repo"
    exit 1
fi


requested_backend_found=false

for backend_found in $(pkg_db_backends) ; do
    if [ $backend = ${backend_found##*/} ] ; then
        requested_backend_found=true
        break
    fi
done

if [ $requested_backend_found = false ] ; then
    error "Requested backend: $backend, not found"
    exit 1
fi

pkg_db_init "$backend" "${target_repo:-$PWD}"

for pkg in "$@" ; do
    if ! pkg=$(pkg_db_get "$pkg"); then
        error "$PKG_DB_ERROR"
        exit 1
    fi
done

pkg_add_init "$backend" "$target_repo"

for pkg in "$@" ; do
    if ! pkg_add "$pkg" ; then
        error "$PKG_ADD_ERROR"
        exit 1
    fi
done

