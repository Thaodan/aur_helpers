#!/bin/bash -e
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#!/bin/bash -e

#\\include locals.shh
#\\define CLEANUP_SUDO t
#\\import msg.in.sh
#\\import cleanup.in.sh
#\\import repo.config.in.sh
#\\import makepkg.conf.load.gnupg.in.sh

#\\define PKGDEST ""$PWD/PKGDEST""

build_pkg()
{
    mkdir -p @PKGDEST@

    export PKGDEST="@PKGDEST@"

    #cleanup_files="$cleanup_files ${CHROOT:?}/$target_repo_name/$$ ${CHROOT:?}/$target_repo_name/$$.lock"

    makechrootpkg \
        -r "$CHROOT/$target_repo_name" \
        -u \
        -c \
#\\ifndef DISABLE_CCCACHE
        -d "${CCACHE_DIR:-$HOME/.ccache}:/build/.ccache" \
#\\endif
        -l $$ \
        -- \
        "$@" || return $?
    sudo rm -rf "${CHROOT:?}/$target_repo_name/$$"
    sudo rm -rf "${CHROOT:?}/$target_repo_name/$$.lock"

    if [ "$sign" ] ; then
        (
            cd @PKGDEST@
            for pkg in *pkg.tar* ; do
                case $pkg in
                    *.sig) : ;;
                    *)
                        if [ ! -e "$pkg".sig ] ; then
                            gpg --detach-sign --use-agent --no-armor ${GPGKEY+ --local-user "$GPGKEY"} "$pkg"
                        fi
                        ;;
                esac
            done
        )
    fi

    if [ "$add_to_binary" ] ; then
        (
            cd @PKGDEST@
            for pkg in *pkg.tar* ; do
                case $pkg in
                    *.sig) : ;;
                    *)
                        repo-addpkg-bin -n "$target_repo" "$pkg"
                    ;;
                esac
            done
        )
    fi
}


usage()
{
    cat <<EOF
usage: $appname [-c <config>]
description: build-pkg from repo

-c,-n <config>  Repo-Config
-s              Sync build packages after building
-t              Set depend path where depending repositories are found
-a              Add build packages to binary repository
-f              Pass pkgbuild to builder

-h              Show help
EOF
}

opts=ht:r:c:sap:

while getopts $opts arg ; do
    case $arg in
        t) depend_path=$OPTARG:$depend_path;;
        n|c) target_repo="$OPTARG";;
        p) PKGBUILD="$OPTARG" ;;
        s) sign=t;;
        a) add_to_binary=t;;
        h) usage; exit 0;;
        ?) usage; exit 1;;
    esac
done
shift $(( $OPTIND - 1 ))

try_find_confin_gitrepositoryroot

if [ ! -e "${PKGBUILD:-PKGBUILD}" ] ; then
    error "No pkgbuild found in current directory or passed via -p"
    exit 1
fi

for pkg in @PKGDEST@/*; do
    if [ -e "$pkg" ] ; then
        error "Some packages already exist in @PKGDEST@, please remove them and try again"
        exit 1
    fi
done

mkdir -p "@TMP_DIR@"/chroot/repos

if ! depend local.conf ; then
    # shellcheck disable=SC2145
    error "$@APP_DOMAIN@_FILE_NOT_FOUND not found, please create local.conf first"
    exit 1
fi

cp /usr/share/devtools/pacman-extra.conf  "@TMP_DIR@"/pacman.conf #FIXME

# load config
if ! depend "$target_repo" add_repo_pacman_conf; then
    # shellcheck disable=SC2145
    error "$@APP_DOMAIN@_FILE_NOT_FOUND not found"
    exit 1
fi

repo-mkchroot  -n "$target_repo_name" -C "@TMP_DIR@"/pacman.conf

sudo cp "@TMP_DIR@/pacman.conf" "$CHROOT/$target_repo_name/root/etc/pacman.conf"
sudo cp "/etc/makepkg.conf" "$CHROOT/$target_repo_name/root/etc/makepkg.conf"

load_makepkg_conf_gpgkey

build_pkg ${PKGBUILD+ -p $PKGBUILD} "$@"
