#!/bin/bash
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#!/bin/bash
appname=${0##*/}

#\\include msg.in.sh
#\\include config.in.sh

repo_syntax=(
    "repo-%s"
    "repo-%s-repo"
    )



usage()
{
    cat <<EOF
usage: $appname -n <name>
description: create repo by -n(name)

-n Name of the repo that is created

EOF
}

opts=n:h

while getopts $opts arg ; do
    case $arg in
        n) repo_name=$OPTARG;;
        h) usage; exit 0;;
        ?) usage; exit 1;;
    esac
done
shift $(( $OPTIND - 1 ))

config_load "appname"



for repo_dir in ${repo_syntax[*]} ; do
    repo_dir_substed="$(sed "s|%s|$repo_name|" <<< "$repo_dir" )"


    if [ -e "$repo_dir_substed" ] ; then
        error "repo $repo_name already exits"
        exit 1
    fi

    
    mkdir "$repo_dir_substed"
    (
        cd "$repo_dir_substed"
        git init

        for git_remote in ${git_remotes[*]} ; do #TODO
            git remote add $git_remote
        done
    )
done
