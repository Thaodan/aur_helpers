#!/bin/bash
# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0

#\\include locals.shh
#\\include repo.config.in.sh
#\\import cleanup.in.sh
#\\import lock.in.sh
#\\import subvolume.in.sh

cleanup_file="@TMP_DIR@"
pacman_conf="@TMP_DIR@"/pacman.conf
CHROOT_VERSION=v4
export LANG=C

usage()
{
    cat <<EOF
usage: $appname -c <config> or -n <repo_name> [-C <pacman.conf>]
description: create or update (root) chroot for repo

-C           Use custom pacman.conf

-h           Show help
EOF
}


create_chroot()
{
    local chroot_dir="$1"
    shift

    cat <<EOF | sudo --preserve-env sh -
    mkdir -p "$chroot_dir"

    pacstrap -Mc ${pacman_conf:+-C "$pacman_conf"} \
             "$chroot_dir" \
             base-devel \
#\\ifndef DISABLE_CCCACHE
             ccache #FIXME
#\\endif

    printf '%s.UTF-8 UTF-8\n' en_US de_DE > '$chroot_dir/etc/locale.gen'
    echo 'LANG=en_US.UTF-8' > '$chroot_dir/etc/locale.conf'
    echo "$CHROOT_VERSION" > "$chroot_dir/.arch-chroot"

    systemd-machine-id-setup --root="$chroot_dir"
EOF

    arch-nspawn "$chroot_dir" \
                locale-gen
}

update_chroot()
{
    arch-nspawn "$1" \
                pacman -Syu --noconfirm
}

opts=hc:n:C:

while getopts $opts arg ; do
    case $arg in
        c) target_repo="$OPTARG";;
        n) target_repo_name="$OPTARG" ;;
        C) pacman_conf="$OPTARG";;
        h) usage; exit 0;;
        ?) usage; exit 1;;
    esac
done
shift $(( $OPTIND - 1 ))

if ! depend local.conf ; then
    # shellcheck disable=SC2145
    error "$@APP_DOMAIN@_FILE_NOT_FOUND not found, please create local.conf first"
    exit 1
fi


# load config
if [ -z "$target_repo_name" ] ; then
    try_find_confin_gitrepositoryroot
    mkdir -p "@TMP_DIR@"/chroot/repos
    cp /usr/share/devtools/pacman-extra.conf  "@TMP_DIR@"/pacman.conf #FIXME
    if ! depend "$target_repo" add_repo_pacman_conf; then
        # shellcheck disable=SC2145
        error "$@APP_DOMAIN@_FILE_NOT_FOUND not found"
        exit 1
    fi
fi

sudo chown $USER "$CHROOT/$target_repo_name/root.lock" 2>/dev/null || true
lock 8 "$CHROOT/$target_repo_name/root.lock"

if [ ! -e "$CHROOT/$target_repo_name/root" ] ; then
    mkdir -p "$CHROOT/$target_repo_name"

    if is_btrfs "$CHROOT/$target_repo_name"; then
	if ! sudo btrfs subvolume create "$CHROOT/$target_repo_name/root"; then
	    error "Couldn't create subvolume for '%s'" "$CHROOT/$target_repo_name/root"
            exit 1
	fi
	sudo chmod 0755 "$CHROOT/$target_repo_name/root"
    fi

    create_chroot "$CHROOT/$target_repo_name/root"
else
    update_chroot "$CHROOT/$target_repo_name/root"
fi

unlock 8
