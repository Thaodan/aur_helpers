# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
include $(abspath $(dir $(lastword $(MAKEFILE_LIST))))/../rules.mk
srcdir			= $(DIST_ROOT)/src
config_shh		= $(srcdir)/config.shh
override SHPPFLAGS		+= -I$(srcdir)/lib \
			-I$(srcdir)/shpp.local\
			-M$(srcdir)/shpp.local \
			-DPREFIX=$(PREFIX) \
			-Dlibdir=$(libdir)/$(APPNAME)
