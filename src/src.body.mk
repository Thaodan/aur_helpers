# Copyright 2018 - 2020, Björn Bidar (arch-linux-)repo-tools
# SPDX-License-Identifier: GPL-2.0
#$(config_shh)

UTILS	+= $(srcdir)/lib/msg.in.sh \
	$(srcdir)/lib/config.in.sh


$(SCRIPTS): ${SCRIPTS:=.in.sh} $(UTILS)
	$(SHPP) $(SHPPFLAGS)\
		$(@).in.sh \
		-o $@


_clean:
	rm -f $(SCRIPTS)
clean: _clean

.PHONY: clean _clean
